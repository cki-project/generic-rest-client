# Generic REST API client

Create a library for your REST service by defining the endpoint models as dataclasses.

Simply define the objects available on the server, and the methods available.

```python
# objects.py

class UserEmail(RESTObject):
    _id = 'email_id'


class UserEmailManager(RESTManager, GETMethod, LISTMethod):
    _obj_cls = UserEmail
    _path = 'user/{user_id}/email'
    _listed_by = 'emails'
    _from_parent_attrs = ('user_id',)


class User(RESTObject):
    _id = 'user_id'
    _managers = {'email': UserEmail}

class UserManager(RESTManager, GETMethod, LISTMethod, CREATEMethod):
    _path = 'user'
    _listed_by = 'users'  #  {'results': {'users': [here the data}}
                          #                  ^--- look for this key
```

```python
# client.py

from restclient import APIManager
import objects

class MyApp:

    def __init__(self, host, token):
        api = APIManager(host, token)

        self.user = objects.UserManager(api)
```

```python
In [1]: import client

In [2]: myapp = client.MyApp('http://0.0.0.0:8888', 'my-client-token')

In [3]: myapp.user.list()
# 127.0.0.1 - - [05/Apr/2020 21:45:44] "GET /user HTTP/1.1" 200 -
Out[3]: [User(user_id=0, name='Bob'), User(user_id=1, name='Alice')]

In [4]: alice = myapp.user.get(1)
# 127.0.0.1 - - [05/Apr/2020 21:45:56] "GET /user/1 HTTP/1.1" 200 -

In [5]: alice.email.list()
# 127.0.0.1 - - [05/Apr/2020 21:46:04] "GET /user/1/email HTTP/1.1" 200 -
Out[5]:
[UserEmail(email_id=1, address='alice@mail.com'),
 UserEmail(email_id=2, address='alice_other@mail.com')]

In [6]: myapp.user.create(name='Joey')
# 127.0.0.1 - - [05/Apr/2020 21:46:40] "POST /user HTTP/1.1" 200 -
Out[6]: User(user_id=2, name='Joey')

In [7]: myapp.user.list()
# 127.0.0.1 - - [05/Apr/2020 21:46:46] "GET /user HTTP/1.1" 200 -
Out[7]:
[User(user_id=0, name='Bob'),
 User(user_id=1, name='Alice'),
 User(user_id=2, name='Joey')]
```
