import unittest
import warnings

import responses

from example_app import MyApp
from example_app import objects
from restclient import RESTManagerError
from restclient import base

USERS = {
        0: {
            'id': 0,
            'name': 'Bob',
            'emails': {
                0: {'email_id': 0, 'address': 'bob@mail.com'},
                3: {'email_id': 3, 'address': 'bob.s@protonmail.com'},
            },
            'phones': [
                {'phone_id': 0, 'number': 5656456},
            ],
            'misc': {'uuid': '4141f2f2-727c-0d31-fd9f-b9d7469a7315'},
        },
        1: {
            'id': 1,
            'name': 'Alice',
            'emails': {
                1: {'email_id': 1, 'address': 'alice@mail.com'},
                2: {'email_id': 2, 'address': 'alice_other@mail.com'},
            },
            'phones': [
                {'phone_id': 1, 'number': 12345},
            ],
            'misc': {'uuid': '1af0e646-4f9f-948d-b070-dd0217d8980f'},
        },
    }


class TestClient(unittest.TestCase):
    # pylint: disable=no-member
    """Test lib methods with example_app/."""

    def setUp(self):
        self.client = MyApp('http://server', None)

    @responses.activate
    def test_list(self):
        """Test LISTMethod."""
        responses.add(
            responses.GET,
            'http://server/user',
            json={
                'results': {'users': list(USERS.values())},
                'count': 2,
                'previous': None,
                'next': None,
            }
        )

        users = self.client.user.list()
        self.assertEqual(list, type(users))
        self.assertEqual(objects.User, type(users[0]))
        self.assertEqual('Bob', users[0].name)
        self.assertEqual(1, len(users[0].phones))

    @responses.activate
    def test_get(self):
        """Test GETMethod."""
        responses.add(responses.GET, 'http://server/user/0', json=USERS[0])

        user = self.client.user.get(0)
        self.assertEqual(objects.User, type(user))
        self.assertEqual('Bob', user.name)
        self.assertEqual(1, len(user.phones))

    @responses.activate
    def test_get_different(self):
        """Test GETMethod."""
        responses.add(responses.GET, 'http://server/user/1', json=USERS[1])

        user = self.client.user.get(1)
        self.assertEqual('Alice', user.name)

    @responses.activate
    def test_get_with_kwarg(self):
        """Test GETMethod with ID as keyword argument."""
        responses.add(responses.GET, 'http://server/user/1', json=USERS[1])

        user = self.client.user.get(id=1)
        self.assertEqual('Alice', user.name)

    @responses.activate
    def test_use_manager_instantiate(self):
        """Test RESTManager.instantiate() method works as expected."""
        mocked_user_uuid = "4141f2f2-727c-0d31-fd9f-b9d7469a7315"
        email_request = responses.get(
            'http://server/user/0/email',
            json={
                'results': {'emails': list(USERS[0]['emails'].values())},
                'count': 2,
                'previous': None,
                'next': None,
            }
        )
        phone_request = responses.get(
            f'http://server/user/{mocked_user_uuid}/phone/0',
            json=USERS[0]['phones'][0],
        )

        # missing misc.uuid
        mocked_user = {'name': 'mocked_user_id', 'id': 0, 'misc': {}}

        # set property referenced by related manager's `_from_parent_attrs`
        with self.subTest("Test an exception will be raised if trying to access"
                          " a related manager missing _from_parent_attrs"):
            user = self.client.user.instantiate(attrs=mocked_user)

            emails = user.email.list()
            self.assertEqual(objects.UserEmail, type(emails[0]))
            self.assertEqual(2, len(emails))
            self.assertEqual('bob@mail.com', emails[0].address)
            self.assertEqual('bob.s@protonmail.com', emails[1].address)
            responses.assert_call_count(email_request.url, 1)

            expected_msg = (r"Missing required attr 'misc\.uuid'"
                            r" to initialize related manager 'UserPhoneManager'")
            with self.assertRaisesRegex(RESTManagerError, expected_msg):
                user.phone.get(0)
            responses.assert_call_count(phone_request.url, 0)

        responses.calls.reset()

        # set property referenced by related manager's `_from_parent_attrs`
        mocked_user = {'name': 'mocked_user_id', 'id': 0, 'misc': {
            'uuid': '4141f2f2-727c-0d31-fd9f-b9d7469a7315'}}

        with self.subTest("Test keeps related managers working"):
            user = self.client.user.instantiate(attrs=mocked_user)

            self.assertEqual(mocked_user['name'], user.name)

            emails = user.email.list()
            self.assertEqual(objects.UserEmail, type(emails[0]))
            self.assertEqual(2, len(emails))
            self.assertEqual('bob@mail.com', emails[0].address)
            self.assertEqual('bob.s@protonmail.com', emails[1].address)

            phone = user.phone.get(0)
            self.assertEqual(objects.UserPhone, type(phone))
            self.assertEqual(5656456, phone.number)

            responses.assert_call_count(email_request.url, 1)
            responses.assert_call_count(phone_request.url, 1)

        responses.calls.reset()

        with self.subTest("Path should be computed and cached the first time the API is hit"):
            user = self.client.user.instantiate(attrs=mocked_user)

            with self.assertLogs(logger=base.LOGGER, level="DEBUG") as log_ctx:
                user.email.list()
                user.email.list()

            expected_log = (
                f"DEBUG:{base.LOGGER.name}:"
                f"Computing path for related manager of {user!r}: {objects.UserEmailManager}"
            )
            self.assertEqual([expected_log], log_ctx.output, "Expected to compute path once")
            responses.assert_call_count(email_request.url, 2)

    @responses.activate
    def test_use_manager_get(self):
        """Test GETMethod in a nested manager."""
        responses.add(responses.GET, 'http://server/user/0', json=USERS[0])
        responses.add(responses.GET, 'http://server/user/0/email/0', json=USERS[0]['emails'][0])
        responses.add(responses.GET, 'http://server/user/0/email/3', json=USERS[0]['emails'][3])

        user = self.client.user.get(0)
        email = user.email.get(0)
        self.assertEqual(objects.UserEmail, type(email))
        self.assertEqual('bob@mail.com', email.address)

        email = user.email.get(3)
        self.assertEqual('bob.s@protonmail.com', email.address)

    @responses.activate
    def test_use_manager_list(self):
        """Test LISTMethod in a nested manager."""
        responses.add(responses.GET, 'http://server/user/0', json=USERS[0])
        responses.add(
            responses.GET,
            'http://server/user/0/email',
            json={
                'results': {'emails': list(USERS[0]['emails'].values())},
                'count': 2,
                'previous': None,
                'next': None,
            }
        )

        user = self.client.user.get(0)
        emails = user.email.list()
        self.assertEqual(objects.UserEmail, type(emails[0]))
        self.assertEqual(2, len(emails))
        self.assertEqual('bob@mail.com', emails[0].address)
        self.assertEqual('bob.s@protonmail.com', emails[1].address)

    @responses.activate
    def test_create(self):
        """Test CREATEMethod."""
        user = {'id': 2, 'name': 'Joey', 'emails': {}, 'phones': [], 'misc': {
            'uuid': 'e54950c4-584c-9ae2-1a27-974ab5557fa1'}}
        responses.add(responses.POST, 'http://server/user', json=user)

        new_user = self.client.user.create(name=user['name'])
        self.assertEqual(user['name'], new_user.name)
        self.assertEqual(user['id'], new_user.id)
        self.assertEqual(user['misc']['uuid'], new_user.misc['uuid'])

    @responses.activate
    def test_delete(self):
        """Test DELETEMethod."""
        responses.add(responses.GET, 'http://server/user/0', json=USERS[0])
        responses.add(responses.DELETE, 'http://server/user/0')

        user = self.client.user.get(0)
        user.delete()


class TestRequests(unittest.TestCase):
    # pylint: disable=no-member
    """Test requests are performed correctly."""

    def setUp(self):
        self.client = MyApp('http://server', 'my-secret-token')

    @responses.activate
    def test_get_params(self):
        responses.add(responses.GET, 'http://server/user/1234', json=USERS[0])

        self.client.user.get(1234, some_param=True, some_other_param=['a', 'b', 'c'])

        self.assertEqual(responses.calls[0].request.headers['Authorization'],
                         'Token my-secret-token')
        self.assertEqual(responses.calls[0].request.headers['Content-Type'], 'application/json')
        self.assertEqual(
            responses.calls[0].request.url,
            ('http://server/user/1234?some_param=True&some_other_param=a'
             '&some_other_param=b&some_other_param=c')
        )

    @responses.activate
    def test_list_params(self):
        responses.add(
            responses.GET,
            'http://server/user',
            json={
                'results': {'users': []},
                'count': 0,
                'previous': None,
                'next': None,
            }
        )

        self.client.user.list(some_param=True, some_other_param=['a', 'b', 'c'])

        self.assertEqual(responses.calls[0].request.headers['Authorization'],
                         'Token my-secret-token')
        self.assertEqual(responses.calls[0].request.headers['Content-Type'], 'application/json')
        self.assertEqual(
            responses.calls[0].request.url,
            ('http://server/user?some_param=True&some_other_param=a'
             '&some_other_param=b&some_other_param=c')
        )

    @responses.activate
    def test_create_params(self):
        responses.add(responses.POST, 'http://server/user', json=USERS[0])

        self.client.user.create(name='Rachel')

        self.assertEqual(responses.calls[0].request.headers['Authorization'],
                         'Token my-secret-token')
        self.assertEqual(responses.calls[0].request.headers['Content-Type'], 'application/json')
        self.assertEqual(responses.calls[0].request.url, 'http://server/user')
        self.assertEqual(responses.calls[0].request.body, '{"name": "Rachel"}')

    @responses.activate
    def test_delete(self):
        responses.add(responses.DELETE, 'http://server/user/1')
        responses.add(responses.GET, 'http://server/user/1', json=USERS[1])

        user = self.client.user.get(1)

        user.delete()

        self.assertEqual(responses.calls[1].request.headers['Authorization'],
                         'Token my-secret-token')
        self.assertEqual(responses.calls[1].request.headers['Content-Type'], 'application/json')
        self.assertEqual(responses.calls[1].request.url, 'http://server/user/1')
        self.assertEqual(responses.calls[1].request.method, 'DELETE')

    @responses.activate
    def test_list_inherited(self):
        """Test elements in a list has the correct inherited parameters."""
        responses.add(
            responses.GET,
            'http://server/user',
            json={
                'results': {'users': list(USERS.values())},
                'count': 2,
                'previous': None,
                'next': None,
            }
        )
        responses.add(
            responses.GET,
            'http://server/user/0/email',
            json={
                'results': {'emails': list(USERS[0]['emails'].values())},
                'count': 2,
                'previous': None,
                'next': None,
            }
        )
        responses.add(
            responses.GET,
            'http://server/user/1/email',
            json={
                'results': {'emails': list(USERS[0]['emails'].values())},
                'count': 2,
                'previous': None,
                'next': None,
            }
        )

        users = self.client.user.list()
        users[0].email.list()
        users[1].email.list()

        self.assertEqual(responses.calls[0].request.url, 'http://server/user')
        self.assertEqual(responses.calls[1].request.url, 'http://server/user/0/email')
        self.assertEqual(responses.calls[2].request.url, 'http://server/user/1/email')

    @responses.activate
    def test_list_as_iterator(self):
        responses.add(
            responses.GET,
            'http://server/user',
            match=[responses.matchers.query_string_matcher("")],
            json={
                'results': {'users': [USERS[0]]},
                'count': 3,
                'previous': None,
                'next': 'http://server/user?offset=1',
            }
        )
        responses.add(
            responses.GET,
            'http://server/user?offset=1',
            json={
                'results': {'users': [USERS[1]]},
                'count': 3,
                'previous': 'http://server/user?offset=0',
                'next': 'http://server/user?offset=2',
            }
        )
        responses.add(
            responses.GET,
            'http://server/user?offset=2',
            json={
                'results': {'users': [USERS[1]]},
                'count': 3,
                'previous': 'http://server/user?offset=1',
                'next': None,
            }
        )

        users = self.client.user.list()
        self.assertEqual(3, len(users))
        self.assertEqual(
            [USERS[0]['id'], USERS[1]['id'], USERS[1]['id']],
            [user.id for user in users]
        )

    @responses.activate
    def test_list_dont_get_next(self):
        responses.add(
            responses.GET,
            'http://server/user',
            match=[responses.matchers.query_string_matcher("")],
            json={
                'results': {'users': [USERS[0]]},
                'count': 3,
                'previous': None,
                'next': 'http://server/user?offset=1',
            }
        )

        users = self.client.user.list(get_next=False)
        self.assertEqual(1, len(users))

    @responses.activate
    def test_list_as_list(self):
        responses.add(
            responses.GET,
            'http://server/user',
            match=[responses.matchers.query_string_matcher("")],
            json={
                'results': {'users': [USERS[0]]},
                'count': 3,
                'previous': None,
                'next': 'http://server/user?offset=1',
            }
        )

        with warnings.catch_warnings(record=True) as warns:
            users = self.client.user.list(as_list=False, get_next=False)
            self.assertEqual(len(warns), 1)
            self.assertIn(
                "as_list=False' parameter is deprecated. Please use 'iterator=True'",
                str(warns[0].message)
            )

        self.assertTrue(isinstance(users, base.List))
        self.assertEqual(1, len(users))

    @responses.activate
    def test_list_iterator(self):
        responses.add(
            responses.GET,
            'http://server/user',
            match=[responses.matchers.query_string_matcher("")],
            json={
                'results': {'users': [USERS[0]]},
                'count': 3,
                'previous': None,
                'next': 'http://server/user?offset=1',
            }
        )

        users = self.client.user.list(iterator=True, get_next=False)
        self.assertTrue(isinstance(users, base.List))
        self.assertEqual(1, len(users))
